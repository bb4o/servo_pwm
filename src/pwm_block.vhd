library ieee;
use ieee.std_logic_1164.all;

entity pwm_block is
	generic(
		gClk: integer := 50000000
	);
	port (
		iClk: in std_logic;
		iReset: in std_logic;
		
		iServo_reg: in std_logic_vector;
		
		oPwm: out std_logic
	);
end entity;


architecture v1 of pwm_block is

	component pwm_translate--0.6-2.3ms
		generic(
			gClk: integer := 50000000;
			gMin: real := 0.6;
			gMax: real := 2.3
		);
		port (
			iClk: in std_logic;
			iReset: in std_logic;
			
			iReg: in std_logic_vector;
			oCnt_cap: out std_logic_vector
		);
	end component;
	
	component pwm_cnt
		generic (
			gClk: integer := 50000000;
			gPeriod: real := 15.0
		);
		port (
			iClk: in std_logic;
			iReset: in std_logic;
			
			iCnt_cap: std_logic_vector;
			
			oPwm: out std_logic
		);
	end component;
	
	signal sCnt_cap: std_logic_vector (iServo_reg'range);

begin

	translate: pwm_translate--0.6-2.3ms
		port map (
			iClk => iClk,
			iReset => iReset,
			
			iReg => iServo_reg,
			oCnt_cap => sCnt_cap
		);
	
	cnt: pwm_cnt
		port map (
			iClk => iClk,
			iReset => iReset,
			
			iCnt_cap => sCnt_cap,
			
			oPwm => oPwm
		);

end v1;