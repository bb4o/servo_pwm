library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity pwm_cnt is
	generic (
		gClk: integer := 50000000;
		gPeriod: real := 15.0
	);
	port (
		iClk: in std_logic;
		iReset: in std_logic;
		
		iCnt_cap: std_logic_vector;
		
		oPwm: out std_logic
	);
end entity;


architecture v1 of pwm_cnt is

	constant cWidth: integer := iCnt_cap'length;
	
	function get_offset(
		clk: integer;
		period: real;
		w: integer
	) return unsigned is
	
		variable offset: unsigned (w-1 downto 0);
		variable rClk: real;
		variable rClk_period: real;
		variable rCount: real;
		
	begin
		
		--get clk tick in real ms
		rClk := real(clk);
		rClk_period := (1000000000.0/rClk)/1000000.0;
		
		--get number of ticks equal to period
		rCount := period/rClk_period +1.0;
		offset := to_unsigned(integer(rCount),w);
		report "period = " & real'image(rCount) & " ticks";
		
		return offset;
		
	end get_offset;
	
	constant cCap: unsigned (cWidth-1 downto 0) := get_offset(gClk,gPeriod,cWidth);
	
	signal sCnt: unsigned (cWidth-1 downto 0);
	signal sAssert_pwm: std_logic;
	signal sCnt_reset: std_logic;
	
	signal sCnt_cap: unsigned (cWidth-1 downto 0);
	
begin

	oPwm <= sAssert_pwm;

	process (iClk)
	begin
		if iClk'event and iClk = '1' then
			sCnt_cap <= unsigned(iCnt_cap);
		end if;
	end process;

	process (iClk)
	begin
		if iClk'event and iClk = '1' then
			if iReset = '1' then
				sAssert_pwm <= '0';
			else
				if sCnt < sCnt_cap then
					sAssert_pwm <= '1';
				else
					sAssert_pwm <= '0';
				end if;
			end if;
		end if;
	end process;
	
	process (iClk)
	begin
		if iClk'event and iClk = '1' then
			if iReset = '1' then
				sCnt_reset <= '0';
			else
				if sCnt > cCap then
					sCnt_reset <= '1';
				else
					sCnt_reset <= '0';
				end if;
			end if;
		end if;
	end process;
	
	process (iClk)
	begin
		if iClk'event and iClk = '1' then
			if iReset = '1' then
				sCnt <= (others => '0');
			else
				if sCnt_reset = '1' then
					sCnt <= (others => '0');
				else
					sCnt <= sCnt +1;
				end if;
			end if;
		end if;
	end process;

end v1;
		