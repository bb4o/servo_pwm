library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;


entity pwm_translate is--0.6-2.3ms
	generic(
		gClk: integer := 50000000;
		gMin: real := 0.6;
		gMax: real := 2.3
	);
	port (
		iClk: in std_logic;
		iReset: in std_logic;
		
		iReg: in std_logic_vector;
		oCnt_cap: out std_logic_vector
	);
end entity;


architecture v1 of pwm_translate is

	constant cWidth: integer := iReg'length;
	
	type tParams is record
		offset: std_logic_vector (cWidth-1 downto 0);
		mult: std_logic_vector (cWidth-1 downto 0);
	end record;
	
	function get_offset(
		clk: integer;
		min_ms: real;
		max_ms: real;
		w: integer
	) return tParams is
	
		variable offset: std_logic_vector (w-1 downto 0);
		variable rClk: real;
		variable rClk_period: real;
		variable rCount: real;
		
		variable rDelta: real;
		variable mult: std_logic_vector (w-1 downto 0);
		
		variable ret: tParams;
		
	begin
		
		--get clk tick in real ms
		rClk := real(clk);
		rClk_period := (1000000000.0/rClk)/1000000.0;
		
		--get number of ticks equal to min_ms
		rCount := min_ms/rClk_period +1.0;
		offset := std_logic_vector(to_unsigned(integer(rCount),w));
		report "offset = " & real'image(rCount) & " ticks";
		
		--get delta
		rDelta := max_ms-min_ms;
		rCount := rDelta/rClk_period;
		mult := std_logic_vector(to_unsigned(integer(rCount),w));
		report "mult = " & real'image(rCount) & " ticks";
		
		ret.offset := offset;
		ret.mult := mult;
		
		return ret;
		
	end get_offset;
		
	constant cParams: tParams := get_offset(gClk, gMin, gMax, cWidth);
	constant cMult: std_logic_vector (cWidth-1 downto 0) := cParams.mult;
	constant cOffset: std_logic_vector (cWidth-1 downto 0) := cParams.offset;
	
	signal sReg: unsigned (cWidth-1 downto 0);
	
	signal sReg_multed_long: unsigned (cWidth*2-1 downto 0);
	signal sReg_multed: unsigned (cWidth-1 downto 0);
	
	signal sReg_mult_summ: unsigned (cWidth-1 downto 0);
		
begin

--	process (iClk)
--	begin
--		if iClk'event and iClk = '1' then
			oCnt_cap <= std_logic_vector(sReg_mult_summ);
--		end if;
--	end process;

	process (iClk)
	begin
		if iClk'event and iClk = '1' then
			if iReset = '1' then
				sReg <= (others => '0');
			else
				sReg <= unsigned(iReg);
			end if;
		end if;
	end process;
	
	process (iClk)
	begin
		if iClk'event and iClk = '1' then
			sReg_multed_long <= sReg * unsigned(cMult);
		end if;
	end process;
	
	sReg_multed <= sReg_multed_long (cWidth*2-1 downto cWidth);
	
	process (iClk)
	begin
		if iClk'event and iClk = '1' then
			sReg_mult_summ <= sReg_multed+unsigned(cOffset);
		end if;
	end process;

end v1;